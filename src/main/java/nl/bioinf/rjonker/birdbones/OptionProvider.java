package nl.bioinf.rjonker.birdbones;

/**
 * Copyright (c) 2018 Rinze-Pieter Jonker.
 * Licensed under GPLv3. See gpl.md
 */

public interface OptionProvider {

    String getOutputFile();

    String getUnknownFile();

}
