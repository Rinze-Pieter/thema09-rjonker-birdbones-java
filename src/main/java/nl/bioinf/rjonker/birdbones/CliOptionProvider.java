package nl.bioinf.rjonker.birdbones;

/**
 * Copyright (c) 2018 Rinze-Pieter Jonker.
 * Licensed under GPLv3. See gpl.md
 */

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import weka.core.pmml.jaxbbindings.True;

/**
 * this class provides the arguments from the commandline and processes them
 */
public class CliOptionProvider implements OptionProvider {
    private static final String HELP = "help";
    private static final String UNKNOWNFILE = "unknownfile";
    private static final String OUTPUTFILE = "outputfile";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;


    public CliOptionProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * This function checks if somebody requested help, if this is true the function will return a true
     * @return
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * this is the function that you can call to run everything
     */
    private void initialize() {
        buildOptions();
        processCommandline();
    }

    /**
     * this function processes the commandline and updates the commandline variable
     */
    private void processCommandline() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
        } catch (ParseException ex){
            throw new IllegalArgumentException(ex);
        }

    }

    /**
     * this function builds the options so that the script recognises them in the commandline
     */
    public void buildOptions (){
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "prints this message");
        Option unknownOption = new Option("u", UNKNOWNFILE, true, "File that you want to clasify (only Arff at this moment), example: unknownfile.arff");
        Option outputOption = new Option("o", OUTPUTFILE,true, "the File where the results are going to be written (Only Arff at this moment), Example: result.arff");
        unknownOption.setRequired(true);
        outputOption.setRequired(true);

        options.addOption(helpOption);
        options.addOption(unknownOption);
        options.addOption(outputOption);
    }

    /**
     * this function prints the help for all commandline parser
     */
    public void printHelp(){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Birdbone Clasifier", options);
    }


    @Override
    public String getOutputFile() {
        return  this.commandLine.getOptionValue(OUTPUTFILE);
    }

    @Override
    public String getUnknownFile() {
        return this.commandLine.getOptionValue(UNKNOWNFILE);
    }
}
