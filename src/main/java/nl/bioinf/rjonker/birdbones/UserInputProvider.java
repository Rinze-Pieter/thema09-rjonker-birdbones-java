package nl.bioinf.rjonker.birdbones;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInputProvider implements OptionProvider {
    private String outputFile;
    private String unknownFile;

    public UserInputProvider(){
        initialize();
    }

    private void initialize() {
        Scanner scanner = new Scanner(System.in);
        fetchOutputfile(scanner);
        fetchUnknownFile(scanner);
    }

    private void fetchUnknownFile(final Scanner scanner) {
        String pathToUnknown = "/data";
        System.out.println("Please enther the path to the unknownfile");
        pathToUnknown = scanner.next();
        this.unknownFile = pathToUnknown;
    }

    private void fetchOutputfile(final Scanner scanner) {
        String pathToOutput = "/data";
        System.out.println("Please enter the path to the outputfile");
        pathToOutput = scanner.next();
        this.outputFile = pathToOutput;

    }


    @Override
    public String getOutputFile() {
        return outputFile;
    }

    @Override
    public String getUnknownFile() {
        return unknownFile;
    }
}
