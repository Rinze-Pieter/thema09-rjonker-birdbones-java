package nl.bioinf.rjonker.birdbones;

/**
 * Copyright (c) 2018 Rinze-Pieter Jonker.
 * Licensed under GPLv3. See gpl.md
 */

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.*;

/**
 * this class is a classifier that classifies birds skeletons on the data of their bones
 */
public class BirdClassifier {
    private final String modelFile = "data/Bird_RandomForest.model";


    public void start(String unknownFile, String outputFile) {
        try {
            Path path = Paths.get("data/Bird_RandomForest.model ...");
            if (Files.notExists(path)){
                String trainingFile = "data/Bird_without_na.arff";
                Instances instances = loadArff(trainingFile);
                RandomForest randomForest = buildClassifier(instances);
                saveClassifier(randomForest);
            }
            RandomForest fromFile = loadClassifier();

            Instances unknownInstances = loadArff(unknownFile);


            classifyNewInstance(fromFile, unknownInstances, outputFile);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This function classifies the unknown instances that are in the unknownInstances variable
     * @param tree: this is the tree that was made in one the buildClassifier function
     * @param unknownInstances: this is the variable that has all the instances that have no classification
     * @param outputFile :this is the outputfile where the data is gonna be written to
     */
    private void classifyNewInstance(RandomForest tree, Instances unknownInstances, String outputFile) throws Exception {
        Instances labeled = new Instances(unknownInstances);
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }

        PrintWriter writer = new PrintWriter(outputFile);
        writer.println(labeled.toString());
        writer.close();
    }

    /**
     * this function loads the classifier form the data folder
     * @return a working classifier that can be used in the java script
     * @throws Exception
     */
    private RandomForest loadClassifier() throws Exception {
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * this function saves the RandomForest classifier ot the data folder
     * @param randomForest: this is the classifier that was made in one of the earlier functions
     * @throws Exception
     */
    private void saveClassifier(RandomForest randomForest) throws Exception {
        weka.core.SerializationHelper.write(modelFile, randomForest);
    }

    /**
     * this function builds a classifier if there is none
     * @param instances: these are the instances gotten form the training data set
     * @return
     * @throws Exception
     */
    private RandomForest buildClassifier(Instances instances) throws Exception {
        RandomForest tree = new RandomForest();         // new instance of tree
        tree.buildClassifier(instances);   // build classifier
        return tree;
    }

    /**
     * this function loads the data from a arff file
     * @param datafile: the path to the arff file
     * @return a instances datatype with all the data in it
     * @throws IOException
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            data.deleteAttributeAt(1);
            data.deleteAttributeAt(0);
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}