package nl.bioinf.rjonker.birdbones;

/**
 * Copyright (c) 2018 Rinze-Pieter Jonker.
 * Licensed under GPLv3. See gpl.md
 */

import java.util.Arrays;

/**
 * this class runs the WekaClassfier.java script with the arguments gotten form the CliOptionProvider.java script
 */
public final class ArgsRunner {

    private ArgsRunner() {
    }

    public static void main(String[] args) {
        try {
            CliOptionProvider optionProvider = new CliOptionProvider(args);
            if  (optionProvider.helpRequested()) {
                optionProvider.printHelp();
                return ;
            }

            BirdClassifier birdClassifier = new BirdClassifier();
            birdClassifier.start(optionProvider.getUnknownFile(), optionProvider.getOutputFile());


        } catch (IllegalStateException ex) {
            System.out.println("The given commandline arguments weren't valid " +
                    Arrays.toString(args));
            System.out.println("Parsing faild, Reason:" + ex.getMessage());
            CliOptionProvider optionProvider = new CliOptionProvider(new String[]{});
            optionProvider.printHelp();
        }
    }
}
