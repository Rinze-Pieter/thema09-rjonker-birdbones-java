# Bird bone classfier

This is a java classifier that classifies bird skeletons on basis of the lenght of some of their bones and the diameter
of some of their bones, these bones are the Humerus, Femur, Ulna, Tibiotarsus and the Tarsometatarsus. The data from these 
bones are given in mm. The birds will be classified into 5 different classes, these classes where chosen on the landscape where
the bird lived in or the type of bird it was. The types of birds are Singing (SO), Swimming (SW), Wadding (W),
Terrestrial (T), Raptors (R) and Scansorial (P).

## Installation

Clone this repository in to the location that you want.

```pip
git clone https://Rinze-Pieter@bitbucket.org/Rinze-Pieter/thema09-rjonker-birdbones-java.git
```

## Usage

```pip
ArgsRunner.java -u <Unknownfile> -o <Outputfile>
```

For the file you need to put in the -u needs to be an .arff file and of the bird skeletons that you want to know the 
classification of, in this file the classification of the bird should be a "?". This can be seen in the file "data/UnknownBirds.arff".
 
 -o is the location of the file where the results will be written to, this is also an .arff file and if 
the file allready exists the file will be overwritten.

If you need help with the code you will need to use the -h argument, this will give you all the documentation that you will
need to run this file.

### Example

```pip
ArgsRunner.java -u "data/UnknownBirds.arff" -o "data/Result.arff"
```

The script above will run the script with "data/Bird_without_na.arff" as the file it needs to classify and with "data/Result.arff"
as the output file.

## Files included

* data/UnknownBirds.arff - this file is used so that the classifier can be tested and as a template for what the arff file should look like.
* data/Bird_without_na.arff - this is the file that is used to train the classifier and create the model if the model is missing.
* data/Bird_RandomForest.model - this is the file for the machine learning model that is used to classify the bird skeletons.


## Roadmap

i'm looking to add a funtion in to the script that makes it possible to add your own algortihm and to make it possible to 
read .csv files instead of only .arff files.

## Links

The Bitbucket Repository : [https://bitbucket.org/Rinze-Pieter/thema09-rjonker-birdbones-java/src/master/](https://bitbucket.org/Rinze-Pieter/thema09-rjonker-birdbones-java/src/master/)

## Author

* Rinze-Pieter Jonker
* 345258
* BFV3